
/*
	       OA TEMPLATE 4.0
	----------------------------
	Desenvolvido em CoffeeScript

		Código: Fabiane Lima
 */

(function() {
  var imgs, preload;

  imgs = ['assets/img/1pasta.png', 'assets/img/2pasta.png', 'assets/img/audio-off.svg', 'assets/img/audio.svg', 'assets/img/bg-modal.jpg', 'assets/img/bg.jpg', 'assets/img/bt-o.png', 'assets/img/bt.png', 'assets/img/caixa.png', 'assets/img/pasta.png'];

  preload = function(imgs) {
    var counter;
    counter = 0;
    $(imgs).each(function() {
      $('<img />').attr('src', this).appendTo('body').css({
        display: 'none'
      });
      return counter++;
    });
    if (counter === imgs.length) {
      $('main').css({
        opacity: '1'
      });
      return $('body').css({
        background: '#e7e7e7'
      });
    }
  };

  $(window).on('load', function() {
    return preload(imgs);
  });

  $(function() {
    var alt, ans, count, ctrl, data1, data2, endit, fase, func, score, sound, sounds;
    alt = void 0;
    count = 0;
    score = 0;
    fase = 1;
    ans = {
      c1: 0,
      c2: 0
    };
    sound = false;
    sounds = {
      trilha: new Audio('assets/audio/trilha.mp3'),
      clique: new Audio('assets/audio/clique.mp3'),
      acerto: new Audio('assets/audio/acerto.mp3'),
      erro: new Audio('assets/audio/erro.mp3'),
      fase2: new Audio('assets/audio/fase2.mp3'),
      mais: new Audio('assets/audio/mais.mp3'),
      menos: new Audio('assets/audio/menos.mp3')
    };
    ctrl = [];
    endit = [];
    data1 = [['Receita<br>pública<br>originária', 'Receita<br>pública<br>ordinária', 'Receitas<br>correntes ou<br>ordinárias', 'Receitas<br>de<br>capital'], ['<p>Sentido<br/>amplo</p>', '<p>Sentido<br/>econômico</p>'], ['<h1>Receita pública originária</h1><p>Muito bem, é importante ressaltar que é a receita pública que tem como origem o uso do patrimônio público pelo Estado, como quando obtém receitas de aluguel por imóveis locados a terceiros.</p>', '<h1>Receita pública ordinária</h1><p>Embora não reste dúvida de que é o maior valor de receita recebida pelo Estado, a receita pública derivada é aquela obtida pela tributação do patrimônio da sociedade.</p>', '<h1>Receitas correntes ou ordinárias</h1><p>As receitas correntes são recursos obtidos a partir de atividades próprias do Estado, como a tributação. Essas atividades, também ditas operacionais, permitem o ingresso de recursos que serão aplicados em atividades operacionais, como aquelas definidas no orçamento, como programas governamentais. Um exemplo seriam os recursos obtidos para investimentos em educação e saúde.</p>', '<h1>Receitas de capital</h1><p>A teor do artigo 11 da Lei n. 4.320/64, a Receita se classifica em receitas correntes e receitas de capital.No mesmo artigo, em seu parágrafo 1º, as receitas correntes compreendem as receitas tributária, de contribuições, patrimonial, agropecuária, industrial, de serviços, entre outras.As receitas de capital são indicadas no parágrafo 2º do artigo indicado acima e constituem-se nas receitas provenientes da realização de recursos financeiros oriundos de constituição de dívidas; da conversão, em espécie, de bens e direitos; os recursos recebidos de outras pessoas de direito público ou privado, destinados a atender despesas classificáveis em Despesas de Capital e, ainda, o superávit do Orçamento Corrente.</p>']];
    data2 = [
      {
        enun: 'Faz parte desta receita: impostos, taxas e contribuições de melhorias.',
        alts: ['Tributária', 'de Contribuições', 'Patrimonial', 'Agropecuária', 'Industrial', 'de Serviços', 'Transferência corrente', 'Outras receitas correntes'],
        answ: 0,
        feed: 'A receita tributária compreende o ingresso de recursos que têm origem na arrecadação de tributos, conforme os exemplos já citados.'
      }, {
        enun: 'São exemplos desta receita aquelas destinadas ao custeio da seguridade social, como o PIS, a COFINS, a Contribuição Social sobre o Lucro Líquido, conforme o art. 149 da Constituição Federal de 1988.',
        alts: ['Tributária', 'de Contribuições', 'Patrimonial', 'Agropecuária', 'Industrial', 'de Serviços', 'Transferência corrente', 'Outras receitas correntes'],
        answ: 1,
        feed: 'As denominadas receitas de contribuições são aquelas referentes às contribuições sociais, de intervenção no domínio econômico e de interesse das categorias profissionais ou econômicas, como instrumento de intervenção nas respectivas áreas. Vale dizer que o conceito acima está inserido na Constituição Federal, em seus arts. 149 e 195.'
      }, {
        enun: 'O Estado por vezes obtém receitas pelo uso de ativos de sua propriedade, como aluguéis de prédios, rendimentos de aplicações financeiras etc.',
        alts: ['Tributária', 'de Contribuições', 'Patrimonial', 'Agropecuária', 'Industrial', 'de Serviços', 'Transferência corrente', 'Outras receitas correntes'],
        answ: 2,
        feed: 'Isso mesmo, essas são denominadas de receitas patrimoniais.'
      }, {
        enun: 'O mais comum é a receita dessa categoria vir das taxas cobradas pelo Ministério da Agricultura ou órgão que regula essa atividade.',
        alts: ['Tributária', 'de Contribuições', 'Patrimonial', 'Agropecuária', 'Industrial', 'de Serviços', 'Transferência corrente', 'Outras receitas correntes'],
        answ: 3,
        feed: 'Isso mesmo, é uma Receita de natureza eventual, apenas obtida quando o Estado incorpora recursos relativos a receitas de atividades agropecuárias. Entretanto, o mais comum é a receita dessa categoria vir das taxas cobradas pelo Ministério da Agricultura ou órgão que regula essa atividade, como a Embrapa.'
      }, {
        enun: 'Como exemplos que desta receita, podemos destacar os casos de recebimento de royalties de petróleo destinados aos estados e municípios.',
        alts: ['Tributária', 'de Contribuições', 'Patrimonial', 'Agropecuária', 'Industrial', 'de Serviços', 'Transferência corrente', 'Outras receitas correntes'],
        answ: 4,
        feed: 'Como o Estado brasileiro se afastou do exercício de atividades industriais, deixando-as à iniciativa privada, esse tipo de receita praticamente não existe mais.'
      }, {
        enun: 'A receita provém fundamentalmente da atividade de prestação de serviços pelo Estado no que concerne à fiscalização, inspeção etc., cobrando as taxas para executar essas atividades.',
        alts: ['Tributária', 'de Contribuições', 'Patrimonial', 'Agropecuária', 'Industrial', 'de Serviços', 'Transferência corrente', 'Outras receitas correntes'],
        answ: 5,
        feed: 'Muito bem, a receita de serviços cumpre apenas a atividade de prestação de serviços pelo Estado!'
      }, {
        enun: 'São movimentos de recursos entre União, Estados, Distrito Federal e Municípios.',
        alts: ['Tributária', 'de Contribuições', 'Patrimonial', 'Agropecuária', 'Industrial', 'de Serviços', 'Transferência corrente', 'Outras receitas correntes'],
        answ: 6,
        feed: 'Muito bem, as transferências correntes movimentos de recursos entre União, Estados, Distrito Federal e Municípios, por imposição legal (normalmente via Constituição Federal) ou por convênios estabelecidos entre esses entes públicos.'
      }, {
        enun: 'São tratados como outras receitas correntes, tendo como exemplos multas, cobrança da dívida ativa, indenizações, repasse emergencial a título de crises, como calamidade pública.',
        alts: ['Tributária', 'de Contribuições', 'Patrimonial', 'Agropecuária', 'Industrial', 'de Serviços', 'Transferência corrente', 'Outras receitas correntes'],
        answ: 7,
        feed: 'Isso mesmo, recursos que entram para os cofres do Estado e não têm uma classificação específica, são tratados como outras receitas correntes.'
      }
    ];
    func = {
      audio: function() {
        sounds.clique.play();
        if (sound === false) {
          sound = true;
          sounds.trilha.play();
          return $('.audio').html('<img src="assets/img/audio.svg">');
        } else if (sound === true) {
          sound = false;
          sounds.trilha.pause();
          return $('.audio').html('<img src="assets/img/audio-off.svg">');
        }
      },
      help: function() {
        sounds.clique.play();
        $('.content').fadeOut();
        $('.dimmer').fadeIn();
        if (fase === 1) {
          $('.modal').html('<h1>Ajuda</h1><p>Analise o cenário a seguir e coloque em ordem as categorias da receita orçamentária.</p><button class="dismiss">Fechar</button>');
        }
        if (fase === 2) {
          return $('.modal').html('<h1>Ajuda</h1><p>Leia com atenção cada uma das afirmativas e responda em qual dos níveis se classificam as receitas correntes.</p><button class="dismiss">Fechar</button>');
        }
      },
      info: function() {
        sounds.clique.play();
        return $('.modal').html('<h1>Referência</h1><p></p><button class="end">Voltar</button>');
      },
      end: function() {
        $('.content').fadeOut();
        $('.dimmer').fadeIn();
        if (score >= 7) {
          sounds.mais.play();
          $('.modal').html('<h1>Você acertou ' + score + ' questões!</h1><p>Você chegou ao final das duas etapas, e seu desempenho foi excelente! Agora, você já sabe a classificação da receita orçamentária e os níveis que classificam as receitas correntes.</p><button class="again">Ver novamente</button>');
        }
        if (score >= 5 && score < 7) {
          sounds.menos.play();
          return $('.modal').html('<h1>Você acertou ' + score + ' questões!</h1><p>Você chegou ao final das duas etapas, e seu desempenho foi bom! Entretanto, é preciso estudar mais sobre a classificação da receita orçamentária e os níveis que classificam as receitas correntes.</p><button class="again">Ver novamente</button>');
        } else if (score >= 1 && score <= 4) {
          sounds.menos.play();
          if (score === 1) {
            return $('.modal').html('<h1>Você acertou uma questão!</h1><p>Você chegou ao final das duas etapas, e seu desempenho não foi como esperado! É preciso estudar mais sobre a classificação da receita orçamentária e os níveis que classificam as receitas correntes.</p><button class="again">Ver novamente</button>');
          } else {
            return $('.modal').html('<h1>Você acertou ' + score + ' questões!</h1><p>Você chegou ao final das duas etapas, e seu desempenho não foi como esperado! É preciso estudar mais sobre a classificação da receita orçamentária e os níveis que classificam as receitas correntes.</p><button class="again">Ver novamente</button>');
          }
        } else if (score === 0) {
          sounds.menos.play();
          return $('.modal').html('<h1>Você não acertou nenhuma questão!</h1><p>Você chegou ao final das duas etapas, e seu desempenho não foi como esperado! É preciso estudar mais sobre a classificação da receita orçamentária e os níveis que classificam as receitas correntes.</p><button class="again">Ver novamente</button>');
        }
      },
      dismiss: function() {
        sounds.clique.play();
        $('.content').fadeIn();
        return $('.dimmer').fadeOut();
      },
      prifase: function() {
        var i, j, len, m, ref;
        sound = true;
        sounds.trilha.play();
        sounds.trilha.loop = true;
        sounds.clique.play();
        func.dismiss();
        func.rNumber();
        ref = data1[1];
        for (i = m = 0, len = ref.length; m < len; i = ++m) {
          j = ref[i];
          $('.droppie').append('<div class="c' + (i + 1) + '">' + j + '</div>');
        }
        func.draggie();
        return func.droppie();
      },
      rNumber: function() {
        var num, randy;
        num = void 0;
        randy = Math.floor(Math.random() * data1[0].length);
        if (ctrl.length < data1[0].length) {
          if (ctrl.indexOf(randy) === -1) {
            ctrl.push(randy);
            if (randy === 0 || randy === 1) {
              num = 1;
            } else if (randy === 2 || randy === 3) {
              num = 2;
            }
            $('.draggie').append('<div class="p' + num + '">' + data1[0][randy] + '</div>');
          }
          return func.rNumber();
        }
      },
      draggie: function() {
        return $('.draggie').children().draggable({
          cursor: 'move',
          revert: function(event, ui) {
            this.data('uiDraggable').originalPosition = {
              top: 0,
              left: 0
            };
            !event;
            if (!event) {
              return sounds.erro.play();
            }
          }
        });
      },
      droppie: function() {
        return $('.droppie').children().droppable({
          tolerance: 'touch',
          accept: function(e) {
            if ($(this).hasClass('c1') && e.hasClass('p1')) {
              return true;
            } else if ($(this).hasClass('c2') && e.hasClass('p2')) {
              return true;
            }
          },
          drop: function(e, ui) {
            sounds.acerto.play();
            endit.push($(this).index());
            if (endit.slice(-1)[0] === 0) {
              ans.c1++;
            } else if (endit.slice(-1)[0] === 1) {
              ans.c2++;
            }
            if (ans.c1 === 1) {
              $('.droppie .c1').css({
                background: 'url(assets/img/1pasta.png) no-repeat',
                backgroundSize: 'contain'
              });
            }
            if (ans.c1 === 2) {
              $('.droppie .c1').css({
                background: 'url(assets/img/2pasta.png) no-repeat',
                backgroundSize: 'contain'
              });
            }
            if (ans.c2 === 1) {
              $('.droppie .c2').css({
                background: 'url(assets/img/1pasta.png) no-repeat',
                backgroundSize: 'contain'
              });
            }
            if (ans.c2 === 2) {
              $('.droppie .c2').css({
                background: 'url(assets/img/2pasta.png) no-repeat',
                backgroundSize: 'contain'
              });
            }
            $('.content').fadeOut();
            $('.dimmer').delay(300).fadeIn();
            if ($('.ui-draggable-dragging').html() === data1[0][0]) {
              $('.modal').html(data1[2][0] + '<button class="dismissit">Fechar</button>');
            } else if ($('.ui-draggable-dragging').html() === data1[0][1]) {
              $('.modal').html(data1[2][1] + '<button class="dismissit">Fechar</button>');
            } else if ($('.ui-draggable-dragging').html() === data1[0][2]) {
              $('.modal').html(data1[2][2] + '<button class="dismissit">Fechar</button>');
            } else if ($('.ui-draggable-dragging').html() === data1[0][3]) {
              $('.modal').html(data1[2][3] + '<button class="dismissit">Fechar</button>');
            }
            return $('.ui-draggable-dragging').fadeOut();
          }
        });
      },
      dismissIt: function() {
        func.dismiss();
        if (endit.length === ctrl.length) {
          $('.draggie').fadeOut();
          $('.modal').html('');
          $('.modal').css({
            top: '20%'
          });
          return setTimeout(function() {
            return func.inter();
          }, 700);
        }
      },
      inter: function() {
        fase = 2;
        sounds.clique.play();
        $('.draggie, .droppie').fadeOut();
        $('.content').fadeOut();
        $('.dimmer').fadeIn();
        return $('.modal').html('<h1>Classificação da receita orçamentária</h1><p>Nesta segunda fase, você terá que classificar as receitas correntes, de acordo com o Art 11, da Lei n. 4.320/64. Leia as definições e clique na pasta correspondente!</p><button class="secfase">Prosseguir</button>');
      },
      secfase: function() {
        var j, nrNumber;
        j = 0;
        ctrl = [];
        $('.score').fadeIn();
        sounds.fase2.play();
        nrNumber = function() {
          var randie;
          randie = Math.floor(Math.random() * data2.length);
          if (ctrl.length < data2.length) {
            if (ctrl.indexOf(randie) === -1) {
              ctrl.push(randie);
              $('.quiz').append('<section> <div class="enun"> <p>' + data2[randie].enun + '</p></div> <div class="alts"><ul></ul></div> </section>');
            }
            return nrNumber();
          }
        };
        nrNumber();
        $('img.bg').attr('src', 'assets/img/bg-sec.jpg');
        func.putAlts();
        return func.dismiss();
      },
      selectAlt: function($el) {
        alt = $el.index();
        sounds.clique.play();
        $('.verify').css({
          pointerEvents: 'auto',
          opacity: '1'
        });
        $('.alts li').css({
          fontWeight: 'normal',
          borderTop: '0.1em solid #a96b6b'
        });
        $el.css({
          fontWeight: 'bold',
          borderTop: '0.1em solid #26c3c4'
        });
        if (alt === data2[ctrl[count]].answ) {
          score++;
          sounds.acerto.play();
          $('.score').html(score);
          $('.content').fadeOut();
          $('.dimmer').delay(600).fadeIn();
          return $('.modal').html('<h1>Resposta certa!</h1><p>' + data2[ctrl[count]].feed + '</p><button class="nxt">Próxima</button>');
        } else {
          sounds.erro.play();
          $('.alts li:nth-child(' + (alt + 1) + ')').css({
            color: '#C29797'
          });
          $('.alts li:nth-child(' + (data2[ctrl[count]].answ + 1) + ')').css({
            fontWeight: 'bold',
            color: '#26c3c4'
          });
          return setTimeout(function() {
            $('.quiz').fadeOut();
            return func.nxt();
          }, 800);
        }
      },
      nxt: function() {
        count++;
        sounds.clique.play();
        $('.quiz').fadeIn();
        if (count < data2.length) {
          func.dismiss();
          func.putAlts();
          $('.verify').css({
            pointerEvents: 'none',
            opacity: '0.6'
          });
          $('.quiz section:nth-child(' + count + ')').fadeOut();
          return $('.quiz section:nth-child(' + (count + 1) + ')').fadeIn();
        } else {
          return setTimeout(function() {
            return func.end();
          }, 400);
        }
      },
      putAlts: function() {
        var k, l, len, m, ref, results;
        l = 0;
        ref = data2[count].alts;
        results = [];
        for (m = 0, len = ref.length; m < len; m++) {
          k = ref[m];
          $('.quiz section:nth-child(' + (count + 1) + ') .alts ul').append('<li>' + data2[count].alts[l] + '</li>');
          results.push(l++);
        }
        return results;
      }
    };
    $(document).on('click', '.audio', function() {
      return func.audio();
    });
    $(document).on('click', '.start', function() {
      return func.prifase();
    });
    $(document).on('click', '.help', function() {
      return func.help();
    });
    $(document).on('click', '.info', function() {
      return func.info();
    });
    $(document).on('click', '.end', function() {
      return func.end();
    });
    $(document).on('click', '.dismiss', function() {
      return func.dismiss();
    });
    $(document).on('click', '.dismissit', function() {
      return func.dismissIt();
    });
    $(document).on('click', '.alts li', function() {
      return func.selectAlt($(this));
    });
    $(document).on('click', '.verify', function() {
      return func.verify();
    });
    $(document).on('click', '.nxt', function() {
      return func.nxt();
    });
    $(document).on('click', '.secfase', function() {
      return func.secfase();
    });
    return $(document).on('click', '.again', function() {
      return location.reload();
    });
  });

}).call(this);
